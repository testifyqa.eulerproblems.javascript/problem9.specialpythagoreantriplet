const assert = require('assert');
const findPythagoreanProduct = require('../index.js');

describe('Problem9', function() {
  it('should give the correct product for when a+b+c equals 1000', function() {
    assert.equal(findPythagoreanProduct(), 31875000);
  });
});
