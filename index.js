function findPythagoreanProduct() {
  for(var a = 3; a < 1000; a++) {
    for(var b = a+1; b < 1000; b++) {
      var cSquared = (a*a) + (b*b);
      var c = Math.sqrt(cSquared);

      if(a+b+c == 1000) {
        var product = (a*b*c);
      }
    }
  }
  console.info('When a+b+c = 1000, then the product of a*b*c is: ' + product);
  return product;
}

module.exports = findPythagoreanProduct;